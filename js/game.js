/**
 * houseBuilding
 */

var barracks =
{
    start: function()
    {
        this.maxSoldiers = 5;
        this.soldiers = [];

        this.trackedTargets = [];

        this.base.start.call(this);
    },

    spawnWorker: function()
    {
        var newWorker = workerFactory.spawnWorker(this.transform.position, this.team, true);

        newWorker.assignHouse(this);

        this.soldiers.push(newWorker);

        for(var i = 0; i < this.soldiers.length; i++)
        {
            this.soldiers[i].houseIndex = i;
        }
    },

    IsInRange: function(worker, range)
    {
        if(buildingManager == null)
            return false;

        for(var b = 0; b < buildingManager.teams[this.team].length; b++)
        {
            if(worker.transform.position.distanceTo(buildingManager.teams[this.team][b].transform.position) < 250)
                return true;
        }

        return false;
    },

    update: function(deltaTime)
    {
        this.base.update.call(this, deltaTime);

        this.soldiers = this.soldiers.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.soldiers.length < this.maxSoldiers)
        {
            if(this.spawnQueue.length < this.maxSoldiers - this.soldiers.length)
            {
                this.spawnQueue.push(new workerTask("food", false, this.transform.position, 60, this.team, function()
                {
                    this.spawnWorker()
                }, this));
            }
        }
        else
        {
            for(var i = 0; i < this.spawnQueue.length; i++)
            {
                this.spawnQueue[i].cancelTask();
            }

            this.spawnQueue = [];
        }
    }
} 
 
/**
 * building
 */

var building =
{
    setHealth: function(health)
    {
        this.health = health;
        this.maxHealth = health;
    },

    start: function()
    {
        this.ignoreCollisions = true;
        this.health = 10;
        this.maxHealth = 10;

        if(!buildingManager.allBuildings)
        {
            buildingManager.allBuildings = [];
        }

        buildingManager.allBuildings.push(this);

        this.taskList = [];
        this.spawnQueue = [];

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        Object.defineProperty(this, "team", 
		{
			get: function()
			{
				return this.renderer.subImage;
			},

			set: function(val)
			{
				this.renderer.subImage = val;
			}
		})

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        GameAudio.play("bubble");
    },

    damage: function()
    {
        this.health -= 1;

        this.taskList.push(new workerTask("gem", false, this.transform.position, 60, this.team, function()
        {
            this.health += 1;
        }, this));

        if(this.health <= 0)
        {
            this.destroy();
        }
    },

    update: function(deltaTime)
    {
        this.taskList = this.taskList.filter(function(item)
        {
            return !item.completed;
        });

        for(var i = 0; i < this.taskList.length; i++)
        {
            this.taskList[i].update();
        }

        this.spawnQueue = this.spawnQueue.filter(function(item)
        {
            return !item.completed;
        });

        for(var i = 0; i < this.spawnQueue.length; i++)
        {
            this.spawnQueue[i].update();
        }
    },

    setTeam: function(team)
    {
        this.team = team;
        
        buildingManager.teams[this.team].push(this);
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.taskList.length; i++)
        {
            this.taskList[i].cancelTask();
        }

        if(this.spawnQueue)
        {
            for(var i = 0; i < this.spawnQueue.length; i++)
            {
                this.spawnQueue[i].cancelTask();
            }
        }

        buildingManager.teams[this.team].remove(this);

        buildingManager.allBuildings.remove(this);

        this.taskList = [];
    }
} 
 

/**
 * holds building array
 */

var buildingManager =
{
    teams: [[],[],[],[]]
} 
 
/**
 * Buildsite that will transform into a building when completed
 */

var buildSite =
{
    start: function()
    {
        this.targetGems = 10;
        this.gems = 0;

        this.base.start.call(this);

        this.gemSlots = [];

        this.buildingInProgress = false;
    },

    setBuildingType: function(type, image, cost, team, onComplete, context)
    {
        this.targetGems = cost;
        this.targetBuilding = type;
        this.targetBuildingImage = image;
        this.setTeam(team);

        this.onComplete = onComplete;
        this.completeContext = context;

        for(var i = 0; i < cost; i++)
        {
            var offset = (workerFactory.timeSinceStart / 5) + (i / cost) * Math.PI * 2;
            var target = new vector(this.transform.position.x + Math.sin(offset) * 30, this.transform.position.y + Math.cos(offset) * 30);

            var newGemSlot = GameObject.create(gemSlot, target.x, target.y, s_gemslot, 3);
            newGemSlot.setTeam(this.team);

            this.gemSlots.push(newGemSlot);
        }
    },

    update: function(deltaTime)
    {
        var canBuild = true;

        for(var i = 0; i < this.gemSlots.length; i++)
        {
            if(!this.gemSlots[i].contained)
            {
                canBuild = false;
            }
        }

        if(canBuild && !this.buildingInProgress)
        {
            this.buildingInProgress = true;

            for(var i2 = 0; i2 < this.gemSlots.length; i2++)
            {
                var index = i2;
                timingUtil.delay(index * 200, function(index){return function()
                {
                    var scaleUpTween = new TWEEN.Tween(this.gemSlots[index].transform.scale);
                    scaleUpTween.to({x: [1.5, 0], y: [1.5, 0]}, 100);
                    scaleUpTween.easing(TWEEN.Easing.Back.In);
                    scaleUpTween.start();

                    var scaleUpTween2 = new TWEEN.Tween(this.gemSlots[index].contained.transform.scale);
                    scaleUpTween2.to({x: [1.5, 0], y: [1.5, 0]}, 100);
                    scaleUpTween2.easing(TWEEN.Easing.Back.In);
                    scaleUpTween2.start();
                }}(index), this);
            }

            timingUtil.delay(this.gemSlots.length * 100, function()
            {
                var scaleUpTween = new TWEEN.Tween(this.transform.scale);
                scaleUpTween.to({x: [1.5, 0], y: [1.5, 0]}, 800);
                scaleUpTween.easing(TWEEN.Easing.Back.In);
                scaleUpTween.start();

                for(var i = 0; i < this.gemSlots.length; i++)
                {
                    this.gemSlots[i].contained.destroy();
                    this.gemSlots[i].destroy();
                }

                timingUtil.delay(800, function()
                {
                    var building = GameObject.create(this.targetBuilding, this.transform.position.x, this.transform.position.y, this.targetBuildingImage);
                    building.setHealth(this.targetGems * 5);
                    building.setTeam(this.team);
                    this.destroy();

                    this.onComplete.call(this.completeContext, building);
                }, this);
            }, this);
        }
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.gemSlots.length; i++)
            this.gemSlots[i].destroy();
            
        this.base.onDestroy.call(this);
    }
} 
 
/**
 * dead worker
 */

var corpse =
{
    start: function()
    {
        this.name = "skull";
        this.ignoreCollisions = true;

        timingUtil.delay(60 * 3 * 1000, function()
        {
            var scaleUpTween = new TWEEN.Tween(this.transform.scale);
            scaleUpTween.to({x: [1.5, 0], y: [1.5, 0]}, 800);
            scaleUpTween.easing(TWEEN.Easing.Back.In);
            scaleUpTween.start();

            timingUtil.delay(800, function()
            {
                this.destroy();
            }, this);
        }, this);
    }
} 
 
/**
 * farmBuilding
 */

var farmBuilding =
{
    start: function()
    {
        this.targetFood = 4;
        this.foods = 0;

        this.base.start.call(this);

        this.foodSlots = [];
    },

    setTeam: function(team)
    {
        this.base.setTeam.call(this, team);

        for(var i = 0; i < this.targetFood; i++)
        {
            var offset = (workerFactory.timeSinceStart / 5) + (i / this.targetFood) * Math.PI * 2;
            var target = new vector(this.transform.position.x + Math.sin(offset) * 30, this.transform.position.y + Math.cos(offset) * 30);

            var newFoodSlot = GameObject.create(foodSlot, target.x, target.y, s_gemslot, 3);
            newFoodSlot.setTeam(this.team);

            newFoodSlot.storeMode = true;

            this.foodSlots.push(newFoodSlot);
        }
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.foodSlots.length; i++)
            this.foodSlots[i].destroy();
            
        this.base.onDestroy.call(this);
    }
} 
 
/**
 * food for eating
 */

var food =
{
    start: function()
    {
        this.ignoreCollisions = true;

        this.name = "food";
        this.edible = false; //needs to grow first
        this.isHeld = true; //lock for Edible status

        this.growTime = 15; //in seconds
        this.renderer.subImage = 2;
        
        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: 1, y: 1}, this.growTime * 1000);

        scaleUpTween.easing(TWEEN.Easing.Quadratic.InOut);

        scaleUpTween.start();

        timingUtil.delay(this.growTime * 1000, function()
        {
            this.edible = true;
            this.renderer.subImage = 1;
            this.isHeld = false;
        }, this);

        this.shakeLength = 1000;
        this.shakes = 4;
        this.shakeAngle = 45;

        this.minShakeTimer = 5;
        this.maxShakeTimer = 20;
        this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));
        this.currentShakeTimer = 0;
    },

    update: function(deltaTime)
    {
        this.currentShakeTimer += deltaTime;

        if(this.currentShakeTimer >= this.shakeTimer)
        {
            this.shake();

            this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));

            this.currentShakeTimer = 0;
        }
    },

    onDestroy: function()
    {
        foodSpawner.currentActive--;

        foodSpawner.allFood.remove(this);
    },

    shake: function(callback, context)
    {
        var singleShakeTime = this.shakes / this.shakeLength;

        var shakeTween = new TWEEN.Tween(this.transform);

        var angles = [];

        var offset = Math.round(Math.random() * 5);

        for (var i = 0; i < this.shakes; i++)
        {
            var intensiy = 1 - (i / this.shakes);

            // Direction of the shake (is 'i' odd?)
            var dir = !((i + offset) % 2)? -1 : 1;

            var angleRad = this.shakeAngle;

            var angle = angleRad * dir * intensiy;

            angles.push(angle);
        }

        angles.push(0);

        shakeTween.to({rotation: angles}, this.shakeLength);

        shakeTween.easing(TWEEN.Easing.Quadratic.InOut);

        if (callback)
        {
            shakeTween.onComplete.addOnce(callback, context);
        }

        shakeTween.start();
    }
} 
 
/**
 * foodSlot used on farms or storages
 */
var foodSlot = 
{
    start: function()
    {
        this.ignoreCollisions = true;
        this.team = -1;   
        this.task = null;
        this.storeMode = true;
    },

    setTeam: function(team)
    {
        this.team = team;
    },

    update: function(deltaTime)
    {
        if(this.team < 0)
            return;

        if(this.storeMode && this.contained && !this.contained.isStored)
        {
            this.contained = null;
        }

        if(!this.contained)
        {
            this.contained = foodSpawner.spawnFood(this.transform.position);
        }
        
        this.contained.isStored = true;
        this.contained.storedTeam = this.team;

        this.contained.isHeld == !this.contained.edible;
    },

    onDestroy: function()
    {
        if(this.task)
            this.task.cancelTask();
    }
} 
 
/**
 * Food Spawner spawns food in the farms on request
 */
var foodSpawner = 
{
    start: function()
    {
        foodSpawner.allFood = [];

        this.spawnInUpdate = true;
    },

    spawnFood: function(pos)
    {
        var newFood = GameObject.create(food, pos.x, pos.y, s_food, 5);
        foodSpawner.allFood.push(newFood);

        return newFood;
    },

    onDestroy: function()
    {
        for (var i = 0; i < foodSpawner.allFood.length; i++)
        {
            foodSpawner.allFood[i].destroy();
        }
    },

    update: function(deltaTime)
    {
        foodSpawner.allFood = foodSpawner.allFood.filter(function(item)
        {
            return !item.destroyed;
        });
    }
} 
 
/**
 * building material
 */

var gem =
{
    start: function()
    {
        this.ignoreCollisions = true;
        
        this.name = "gem";
        
        this.renderer.subImage = Math.floor(Math.random() * this.renderer.image.subImages);

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        this.shakeLength = 1000;
        this.shakes = 4;
        this.shakeAngle = 45;

        this.minShakeTimer = 5;
        this.maxShakeTimer = 20;
        this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));
        this.currentShakeTimer = 0;

        GameAudio.play("bubble");
    },

    update: function(deltaTime)
    {
        this.currentShakeTimer += deltaTime;

        if(this.currentShakeTimer >= this.shakeTimer)
        {
            this.shake();

            this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));

            this.currentShakeTimer = 0;
        }
    },

    onDestroy: function()
    {
        gemSpawner.currentActive--;

        gemSpawner.allGems.remove(this);
    },

    onClick: function()
    {
        this.destroy();
    },

    shake: function(callback, context)
    {
        var singleShakeTime = this.shakes / this.shakeLength;

        var shakeTween = new TWEEN.Tween(this.transform);

        var angles = [];

        var offset = Math.round(Math.random() * 5);

        for (var i = 0; i < this.shakes; i++)
        {
            var intensiy = 1 - (i / this.shakes);

            // Direction of the shake (is 'i' odd?)
            var dir = !((i + offset) % 2)? -1 : 1;

            var angleRad = this.shakeAngle;

            var angle = angleRad * dir * intensiy;

            angles.push(angle);
        }

        angles.push(0);

        shakeTween.to({rotation: angles}, this.shakeLength);

        shakeTween.easing(TWEEN.Easing.Quadratic.InOut);

        if (callback)
        {
            shakeTween.onComplete.addOnce(callback, context);
        }

        shakeTween.start();
    }
} 
 
/**
 * gemSlot used on buildsites or storages
 */
var gemSlot = 
{
    start: function()
    {
        this.ignoreCollisions = true;
        this.team = -1;   
        this.task = null;
        this.storeMode = false;
    },

    setTeam: function(team)
    {
        this.team = team;

        this.task = new workerTask("gem", this.storeMode, this.transform.position, 15, this.team, function()
        {
            this.contained = this.task.assignedWorker.heldResource;
            this.contained.transform.position = this.transform.position;

            this.contained.isHeld = true;

            timingUtil.delay(100, function()
            {
                this.contained.isHeld = !this.storeMode;
            }, this);

            this.contained.isStored = this.storeMode;
            this.contained.storedTeam = this.team;

            if(!this.storeMode)
                this.task = null;
        }, this, true);
    },

    update: function(deltaTime)
    {
        if(this.task)
            this.task.blockStorage = this.storeMode;

        if(this.storeMode && this.contained && !this.contained.isStored)
        {
            this.contained = null;
            this.task.completed = false;
        }

        if(this.task && !this.contained)
        {
            this.task.update();
        }
    },

    onDestroy: function()
    {
        if(this.task)
            this.task.cancelTask();
    }
} 
 
/**
 * Gem Spawner spawns gems in the world
 */
var gemSpawner = 
{
    start: function()
    {
        this.maxGems = 50;
        this.startGems = 25;

        gemSpawner.allGems = [];
        gemSpawner.wildGems = [];

        this.spawnInUpdate = true;
    },

    spawnGem: function(delay, enableSpawningAfter)
    {
        this.spawnInUpdate = false;

        delay = delay || 0;

        if(gemSpawner.allGems && gemSpawner.allGems.length > 0)
        {
            gemSpawner.allGems = gemSpawner.allGems.filter(function(item)
            {
                return !item.destroyed;
            });

            gemSpawner.wildGems = gemSpawner.allGems.filter(function(item)
            {
                return !item.destroyed && !item.isHeld && !item.isStored;
            });
        }

        timingUtil.delay(delay, function()
        {
            if (gemSpawner.wildGems.length < this.maxGems)
            {
                var pos = new vector(0, 0);
                var posFound = false;
                var tries = 0;

                while(!posFound)
                {
                    if(tries > 10)
                    {
                        this.spawnInUpdate = true;
                        return;
                    }

                    tries++;
                    pos.x = Math.random() * (GameWindow.width);
                    pos.y = Math.random() * (GameWindow.height);

                    var base = buildingManager.teams.random().random().transform.position;

                    pos = pos.add(base);
                    pos.x -= GameWindow.width/2;
                    pos.y -= GameWindow.height/2;

                    posFound = true;

                    if(gemSpawner.wildGems.length > 0 && Math.random() > 0.05)
                    {
                        var gemStart = gemSpawner.wildGems.random();

                        pos.x = - 30 + Math.random() * 60;
                        pos.y = - 30 + Math.random() * 60;

                        pos = pos.normalize();

                        pos = pos.stretch(25);
                        pos = pos.add(gemStart.transform.position);
                    }

                    /*if(pos.x < 0 || pos.y < 0 || pos.x > GameWindow.width || pos.y > GameWindow.height)
                        posFound = false;*/

                    for(var i = 0; i < gemSpawner.wildGems.length; i++)
                    {
                        if(gemSpawner.wildGems[i].transform.position.distanceTo(pos) < 24)
                            posFound = false;
                    }

                    for(var i = 0; i < buildingManager.allBuildings.length; i++)
                    {
                        if(buildingManager.allBuildings[i].transform.position.distanceTo(pos) < 150)
                            posFound = false;
                    }
                }

                gemSpawner.spawnGemAtPosition(pos);
            }

            if (enableSpawningAfter)
            {
                this.spawnInUpdate = true;
            }
        }, this);
    },

    spawnGemAtPosition: function(pos)
    {
        var ngem = GameObject.create(gem, pos.x, pos.y, s_gem, 5);
        gemSpawner.allGems.push(ngem);
        return ngem;
    },

    onDestroy: function()
    {
        for (var i = 0; i < gemSpawner.allGems.length; i++)
        {
            gemSpawner.allGems[i].destroy();
        }
    },

    update: function(deltaTime)
    {
        if (this.spawnInUpdate)
        {
            var percentage = gemSpawner.wildGems.length / this.maxGems;
            this.spawnGem((percentage * percentage * 500), true);
        }
    }
} 
 
/**
 * gemStorageBuilding
 */

var gemStorageBuilding =
{
    start: function()
    {
        this.targetGems = 6;
        this.gems = 0;

        this.base.start.call(this);

        this.gemSlots = [];
    },

    setTeam: function(team)
    {
        this.base.setTeam.call(this, team);

        for(var i = 0; i < this.targetGems; i++)
        {
            var offset = (workerFactory.timeSinceStart / 5) + (i / this.targetGems) * Math.PI * 2;
            var target = new vector(this.transform.position.x + Math.sin(offset) * 30, this.transform.position.y + Math.cos(offset) * 30);

            var newGemSlot = GameObject.create(gemSlot, target.x, target.y, s_gemslot, 3);
            newGemSlot.setTeam(this.team);

            newGemSlot.storeMode = true;

            this.gemSlots.push(newGemSlot);
        }
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.gemSlots.length; i++)
            this.gemSlots[i].destroy();
            
        this.base.onDestroy.call(this);
    }
} 
 
/**
 * graveyardBuilding to store skulls in
 */

var graveyardBuilding =
{
    start: function()
    {
        this.targetSkulls = 6;
        this.skulls = 0;

        this.base.start.call(this);

        this.skullSlots = [];
    },

    setTeam: function(team)
    {
        this.base.setTeam.call(this, team);

        for(var i = 0; i < this.targetSkulls; i++)
        {
            var offset = (workerFactory.timeSinceStart / 5) + (i / this.targetSkulls) * Math.PI * 2;
            var target = new vector(this.transform.position.x + Math.sin(offset) * 30, this.transform.position.y + Math.cos(offset) * 30);

            var newSkullSlot = GameObject.create(skullSlot, target.x, target.y, s_gemslot, 3);
            newSkullSlot.setTeam(this.team);

            newSkullSlot.storeMode = true;

            this.skullSlots.push(newSkullSlot);
        }
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.skullSlots.length; i++)
            this.skullSlots[i].destroy();
            
        this.base.onDestroy.call(this);
    }
} 
 
/**
 * houseBuilding
 */

var houseBuilding =
{
    start: function()
    {
        this.maxWorkers = 5;
        this.workers = [];

        this.base.start.call(this);
    },

    spawnWorker: function()
    {
        var newWorker = workerFactory.spawnWorker(this.transform.position, this.team);

        newWorker.assignHouse(this);

        this.workers.push(newWorker);

        for(var i = 0; i < this.workers.length; i++)
        {
            this.workers[i].houseIndex = i;
        }
    },

    update: function(deltaTime)
    {
        this.base.update.call(this, deltaTime);

        this.workers = this.workers.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.workers.length < this.maxWorkers)
        {
            if(this.spawnQueue.length < this.maxWorkers - this.workers.length)
            {
                this.spawnQueue.push(new workerTask("food", false, this.transform.position, 60, this.team, function()
                {
                    this.spawnWorker()
                }, this));
            }
        }
        else
        {
            for(var i = 0; i < this.spawnQueue.length; i++)
            {
                this.spawnQueue[i].cancelTask();
            }

            this.spawnQueue = [];
        }
    }
} 
 
/**
 * hqBuilding
 */

var hqBuilding =
{
    start: function()
    {
        this.maxWorkers = 8;
        this.workers = [];

        this.base.start.call(this);

        this.buildSites = [];
        this.houses = [];
        this.barracks = [];
        this.gemStores = [];
        this.graveyards = [];
        this.farms = [];
        this.readyForTasks = false;

        this.trackedTargets = [];

        this.buildCooldown = 0;

        timingUtil.delay(1000, function()
        {
            this.spawnWorker();
            timingUtil.delay(1500, function()
            {
                this.readyForTasks = true;
                console.log("ready for tasks");
            }, this);
        }, this);

    },

    IsInRange: function(worker, range)
    {
        if(buildingManager == null)
            return false;

        for(var b = 0; b < buildingManager.teams[this.team].length; b++)
        {
            if(worker.transform.position.distanceTo(buildingManager.teams[this.team][b].transform.position) < range)
                return true;
        }

        return false;
    },

    spawnWorker: function()
    {
        var newWorker = workerFactory.spawnWorker(this.transform.position, this.team);

        newWorker.assignHouse(this);

        this.workers.push(newWorker);

        for(var i = 0; i < this.workers.length; i++)
        {
            this.workers[i].houseIndex = i;
        }
    },

    handleDefense: function()
    {
        var context = this;
        this.trackedTargets = this.trackedTargets.filter(function(item)
        {
            return !item.destroyed && context.IsInRange(item, 450);
        });

        for(var t = 0; t < 4; t++)
        {
            if(t != this.team)
            {
                for(var i = 0; i < workerFactory.teams[t].length; i++)
                {
                    if(this.trackedTargets.indexOf(workerFactory.teams[t][i]) < 0)
                    {
                        if(this.IsInRange(workerFactory.teams[t][i], 150))
                        {
                            this.trackedTargets.push(workerFactory.teams[t][i]);

                            this.taskList.push(new workerAttackTask(workerFactory.teams[t][i], workerFactory.teams[t][i].transform.position, this.team, function()
                            {
                                
                            }, this));
                        }
                    }
                }

                for(var i = 0; i < buildingManager.teams[t].length; i++)
                {
                    if(this.trackedTargets.indexOf(buildingManager.teams[t][i]) < 0)
                    {
                        if(this.IsInRange(buildingManager.teams[t][i], 450))
                        {
                            this.trackedTargets.push(buildingManager.teams[t][i]);

                            for(var s = 0; s < 10; s++)
                            {
                                this.taskList.push(new workerAttackTask(buildingManager.teams[t][i], buildingManager.teams[t][i].transform.position, this.team, function()
                                {
                                    
                                }, this));
                            }
                        }
                    }
                }
            }
        }
    },

    update: function(deltaTime)
    {
        this.buildCooldown -= deltaTime;

        this.base.update.call(this, deltaTime);

        this.buildSites = this.buildSites.filter(function(item)
        {
            return !item.destroyed;
        });

        this.workers = this.workers.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.workers.length < this.maxWorkers && this.readyForTasks)
        {
            if(this.taskList.length < this.maxWorkers - this.workers.length)
            {
                this.taskList.push(new workerTask("food", false, this.transform.position, 60, this.team, function()
                {
                    this.spawnWorker()
                }, this, false));
            }
        }

        this.tryBuild();
        
        if(workerFactory != null && this.barracks.length > 0)
            this.handleDefense();
    },

    tryBuild: function()
    {
        if(this.buildCooldown > 0)
            return;

        if(this.shouldBuildHouse())
        {
            this.makeBuildSite(houseBuilding, s_house, 5, function(newHouse)
            {
                this.houses.push(newHouse);
                this.buildCooldown = 3;
            });
        }

        if(this.shouldBuildBarracks())
        {
            this.makeBuildSite(barracks, s_barracks, 6, function(newHouse)
            {
                this.barracks.push(newHouse);
                this.buildCooldown = 3;
            });
        }

        if(this.shouldBuildGemStorage())
        {
            this.makeBuildSite(gemStorageBuilding, s_gemStorage, 3, function(newHouse)
            {
                this.gemStores.push(newHouse);
                this.buildCooldown = 3;
            });
        }

        if(this.shouldBuildFarm())
        {
            this.makeBuildSite(farmBuilding, s_farm, 2, function(newHouse)
            {
                this.farms.push(newHouse);
                this.buildCooldown = 3;
            });
        }

        if(this.shouldBuildGraveyard())
        {
            this.makeBuildSite(graveyardBuilding, s_graveyard, 4, function(newHouse)
            {
                this.graveyards.push(newHouse);
                this.buildCooldown = 3;
            });
        }
    },

    makeBuildSite: function(type, image, cost, onComplete)
    {
        var position = this.findBuildSite();

        if(!position)
            return;

        var newBuildSite = GameObject.create(buildSite, position.x, position.y, s_base, -1);

        newBuildSite.setBuildingType(type, image, cost, this.team, onComplete, this);

        this.buildSites.push(newBuildSite);
    },

    findBuildSite: function()
    {
        var pos = new vector(0, 0);
        var posFound = false;
        var tries = 0;

        while(!posFound)
        {
            if(tries > 100)
                return null;

            posFound = true;
            tries++;

            var buildingStart = buildingManager.teams[this.team].random();

            pos.x = - 75 + Math.random() * 150;
            pos.y = - 75 + Math.random() * 150;

            pos = pos.normalize();

            pos = pos.stretch(240);
            pos = pos.add(buildingStart.transform.position);

           /* if(pos.x < 200 || pos.y < 200 || pos.x > GameWindow.width - 200 || pos.y > GameWindow.height - 200)
                posFound = false;*/

            for(var i = 0; i < gemSpawner.wildGems.length; i++)
            {
                if(gemSpawner.wildGems[i].transform.position.distanceTo(pos) < 99)
                    posFound = false;
            }

            for(var i = 0; i < buildingManager.allBuildings.length; i++)
            {
                if(buildingManager.allBuildings[i].transform.position.distanceTo(pos) < 239)
                    posFound = false;
            }
        }

        return pos;
    },

    shouldBuildHouse: function()
    {
        this.houses = this.houses.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.workers.length < this.maxWorkers)
            return false;

        for(var i = 0; i < this.houses.length; i++)
        {
            if(this.houses[i].workers.length < this.houses[i].maxWorkers)
                return false;
        }

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == houseBuilding)
                return false;
        }

        return true;
    },

    shouldBuildBarracks: function()
    {
        this.barracks = this.barracks.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.workers.length < this.maxWorkers)
            return false;

        for(var i = 0; i < this.barracks.length; i++)
        {
            if(this.barracks[i].soldiers.length < this.barracks[i].maxSoldiers)
                return false;
        }

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == barracks)
                return false;
        }

        return true;
    },

    shouldBuildGemStorage: function()
    {
        this.gemStores = this.gemStores.filter(function(item)
        {
            return !item.destroyed;
        });

        var workerCount = workerFactory.teams[this.team].length; 
        
        if(workerCount < 3)
            return false;

        for(var i = 0; i < this.gemStores.length; i++)
        {
            for(var s = 0; s < this.gemStores[i].gemSlots.length; s++)
            {
                if(!this.gemStores[i].gemSlots[s].contained)
                    return false;
            }
        }

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == gemStorageBuilding)
                return false;
        }

        return true;
    },

    shouldBuildGraveyard: function()
    {
        this.graveyards = this.graveyards.filter(function(item)
        {
            return !item.destroyed;
        });

        var workerCount = workerFactory.teams[this.team].length; 
        
        if(workerCount < 10)
            return false;

        for(var i = 0; i < this.graveyards.length; i++)
        {
            for(var s = 0; s < this.graveyards[i].skullSlots.length; s++)
            {
                if(!this.graveyards[i].skullSlots[s].contained)
                    return false;
            }
        }

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == graveyardBuilding)
                return false;
        }

        return true;
    },

    setTeam: function(team)
    {
        this.base.setTeam.call(this, team);

        var newFoodSlot = GameObject.create(foodSlot, this.transform.position.x + 35, this.transform.position.y + 35, s_gemslot, 3);
        newFoodSlot.setTeam(this.team);

        newFoodSlot.storeMode = true;
    },

    shouldBuildFarm: function()
    {
        this.farms = this.farms.filter(function(item)
        {
            return !item.destroyed;
        });

        var workerCount = workerFactory.teams[this.team].length; 
 
        var totalSlots = 0;

        for(var i = 0; i < this.farms.length; i++) 
        { 
            totalSlots += this.farms[i].foodSlots.length; 
        }

        if(totalSlots >= (workerCount / 4) * 3) 
            return false;

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == farmBuilding)
                return false;
        }

        return true;
    }
} 
 

/***
 * All images used in the game are  initialized here
 */

 var s_worker = new image("images/snakeHead.png", 4, 4);
 var s_soldier = new image("images/snakeHeadWeapon.png", 4, 4);
var s_food = new image("images/snakeBody.png", 4, 4);
var s_base = new image("images/bases.png", 4, 4);
var s_gem = new image("images/gems.png", 72, 6);
var s_playButton = new image("images/playbutton.png");

var s_playerCountText = new image("images/players.png");
var s_botsOnlyText = new image("images/botsonly.png");
var s_playerCountNumbers = new image("images/playercount.png", 3, 3);


var s_hq = new image("images/buildings/hq.png", 4, 4);
var s_house = new image("images/buildings/house.png", 4, 4);
var s_barracks = new image("images/buildings/barracks.png", 4, 4);
var s_gemStorage = new image("images/buildings/gemstorage.png", 4, 4);
var s_graveyard = new image("images/buildings/graveyard.png", 4, 4);
var s_farm = new image("images/buildings/farm.png", 4, 4);
var s_gemslot = new image("images/gemSlot.png");
var s_skull = new image("images/skull.png"); 
 
/**
 * Main script
 */

window.onload = init;
var pW = 0;
var pH = 0;

function DoInheritance()
{
    buildSite = GameObject.inherit(building, buildSite);
    gemStorageBuilding = GameObject.inherit(building, gemStorageBuilding);
    farmBuilding = GameObject.inherit(building, farmBuilding);
    barracks = GameObject.inherit(building, barracks);
    houseBuilding = GameObject.inherit(building, houseBuilding);
    hqBuilding = GameObject.inherit(building, hqBuilding);
    graveyardBuilding = GameObject.inherit(building, graveyardBuilding);
}

function init()
{
    DoInheritance();
    
    var width = window.innerWidth - 2;
    var height = window.innerHeight - 2;

    Game.initialize(0, 0, width, height, gameStart);
}

function gameStart()
{
	GameWindow.backgroundColor = {r:100, g:149, b:237};

	Game.setUpdateFunction(update);

    GameAudio.init(sounds, soundsFolder);
    GameObject.create(workerFactory, 500, 500);

    var b1 = GameObject.create(hqBuilding, 200, 200, s_hq);
    var b2 = GameObject.create(hqBuilding, GameWindow.width - 200, GameWindow.height - 200, s_hq);
    var b3 = GameObject.create(hqBuilding, 200, GameWindow.height - 200, s_hq);
    var b4 = GameObject.create(hqBuilding, GameWindow.width - 200, 200, s_hq);

    b1.setTeam(0);
    b2.setTeam(1);
    b3.setTeam(2);
    b4.setTeam(3);
    
    GameObject.create(gemSpawner, 500, 500);
    GameObject.create(foodSpawner, 500, 500);

    console.log("STARTED");
}

function update(deltaTime)
{  
   if(GameInput.isHeld(37))
    GameWindow.cameraPosition.x += deltaTime * 200;

    if(GameInput.isHeld(38))
    GameWindow.cameraPosition.y += deltaTime * 200;

    if(GameInput.isHeld(39))
    GameWindow.cameraPosition.x -= deltaTime * 200;

    if(GameInput.isHeld(40))
    GameWindow.cameraPosition.y -= deltaTime * 200;

    if(GameInput.mouseHeld(2))
    {
        GameWindow.cameraPosition = GameWindow.cameraPosition.subtract(new vector(GameInput.trueMousePosition.x - GameWindow.width/2, GameInput.trueMousePosition.y - GameWindow.height/2).stretch(deltaTime * 3));
    }

    if(GameInput.mousePressed(1))
    GameWindow.cameraPosition = new vector(0, 0);

    if(pW != window.innerWidth - 2)
    {
        pW = window.innerWidth - 2;
        GameWindow.g_width = window.innerWidth - 2;
    }

    if(pH != window.innerHeight - 2)
    {
        pH = window.innerHeight - 2;
        GameWindow.g_height = window.innerHeight - 2;
    }
} 
 
/**
 * skullSlot used on storages
 */
var skullSlot = 
{
    start: function()
    {
        this.ignoreCollisions = true;
        this.team = -1;   
        this.task = null;
        this.storeMode = true;
    },

    setTeam: function(team)
    {
        this.team = team;

        this.task = new workerTask("skull", this.storeMode, this.transform.position, 15, this.team, function()
        {
            this.contained = this.task.assignedWorker.heldResource;
            this.contained.transform.position = this.transform.position;

            this.contained.isHeld = true;

            timingUtil.delay(100, function()
            {
                this.contained.isHeld = !this.storeMode;
            }, this);

            this.contained.isStored = this.storeMode;
            this.contained.storedTeam = this.team;

            if(!this.storeMode)
                this.task = null;

        }, this, true);
    },

    update: function(deltaTime)
    {
        if(this.task)
            this.task.blockStorage = this.storeMode;

        if(this.storeMode && this.contained && !this.contained.isStored && this.transform.position.distanceTo(this.contained.transform.position) > 10)
        {
            this.contained = null;
            this.task.completed = false;
        }

        if(this.contained && this.contained.destroyed)
        {
            var ngem = gemSpawner.spawnGemAtPosition(this.transform.position);

            this.contained = ngem;
            this.contained.isStored = false;
            this.contained.team = this.team;
            this.contained.isHeld = false;
        }

        if(this.task && !this.contained)
        {
            this.task.update();
        }
    },

    onDestroy: function()
    {
        if(this.task)
            this.task.cancelTask();
    }
} 
 
/**
 * All sounds in the game
 */

var soundsFolder = "sounds";
var sounds = 
[
	{
        name: "bubble",
        volume: 1
    },
    {
        name: "glass",
        volume: 0.9
    },
    {
        name: "tap",
        volume: 1
    },
    {
        name: "bite",
        volume: 1
    },
    {
        name: "pop",
        volume: 0.1
    }

];  
 
/**
 * Worker
 */

var worker =
{
    start: function()
    {
        this.hunger = 0;
        this.hungerFoodTrigger = 30;
        this.deathHunger = 60;

        this.age = 0;
        this.maxAge = 60 * 5;

        this.ignoreCollisions = true;

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        this.moveSpeed = 100;

        this.task = null;
        this.heldResource = null;
        this.attackTime = 1;
        this.attackTimer = 0;
        this.isSoldier = false;
        
        Object.defineProperty(this, "team", 
		{
			get: function()
			{
				return this.renderer.subImage;
			},

			set: function(val)
			{
				this.renderer.subImage = val;
			}
		})

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        GameAudio.play("bubble");
    },

    assignHouse: function(house)
    {
        this.house = house;
    },

    setTeam: function(team)
    {
        this.team = team;
    },

    update: function(deltaTime)
    {
        this.hunger += deltaTime;
        this.age += deltaTime;
        this.attackTimer -= deltaTime;

        if(this.house && this.house.destroyed)
            this.house = null;

        if(this.house == null)
        {
            for(var i = 0; i < buildingManager.teams[this.team].length; i++)
            {
                if(this.isSoldier)
                {
                    if(buildingManager.teams[this.team][i].soldiers && buildingManager.teams[this.team][i].soldiers.length < buildingManager.teams[this.team][i].maxSoldiers)
                    {
                        buildingManager.teams[this.team][i].soldiers.push(this);
                        this.house = buildingManager.teams[this.team][i];
                    }
                }
                else
                {
                    if(buildingManager.teams[this.team][i].workers && buildingManager.teams[this.team][i].workers.length < buildingManager.teams[this.team][i].maxWorkers)
                    {
                        buildingManager.teams[this.team][i].workers.push(this);
                        this.house = buildingManager.teams[this.team][i];
                    }
                }
            }
        }

        if(this.hunger > this.deathHunger || this.age > this.maxAge)
        {
            this.die();

            return;
        }

        if(this.age > 5 && this.hunger > this.hungerFoodTrigger && !this.task)
        {
            this.task = new workerTask("food", false, this.transform.position, 100, this.team, function()
            {
                this.hunger = 0;
                GameAudio.play("bite");
            }, this, false);

            this.task.selfUpdate = true;
            this.task.assignedWorker = this;
        }

        if(this.task && this.task.selfUpdate)
        {
            this.task.targetPosition = this.transform.position;
            
            this.task.update();
        }

       if(this.heldResource)
       {
           this.heldResource.transform.position = this.heldResource.transform.position.lerpTo(this.transform.position.add(this.transform.forward().stretch(30)), deltaTime * 10);
       }

       if(this.task)
       {
           if(this.task.needWeapon)
           {
                if(this.heldResource)
                {
                    this.drop();
                }

                if(this.transform.position.distanceTo(this.task.targetToAttack.transform.position) > ((this.task.targetToAttack.renderer.image.width / 2) + 24))
                {
                    this.transform.pointTowards(this.task.targetToAttack.transform.position);
                    this.moveForwards(deltaTime * this.moveSpeed * 1.1);
                }
                else
                {
                    if(this.attackTimer < 0)
                    {
                        this.attackTimer = this.attackTime;
                        this.task.targetToAttack.damage();
                    }
                    else
                    {
                        this.attackTimer -= deltaTime;
                    }
                }
           }
           else if(this.heldResource)
           {
               if(this.heldResource.name != this.task.resourceName)
               {
                    this.drop();
               }
               else
               {
                   this.transform.pointTowards(this.task.targetPosition);
                   this.moveForwards(deltaTime * this.moveSpeed);
               }
           }
           else
           {
               var targetResource = this.findResourceByName(this.task.resourceName);

               if(targetResource)
               {
                    this.transform.pointTowards(targetResource.transform.position);

                    if(this.transform.position.distanceTo(targetResource.transform.position) < 20)
                    {
                        this.pickUp(targetResource);
                    }

                    this.moveForwards(deltaTime * this.moveSpeed);
               }
               else
                this.idleBehaviour(deltaTime);
           }
       }
       else
       {
          this.idleBehaviour(deltaTime);
       }
    },

    idleBehaviour: function(deltaTime)
    {
        if(this.house)
        {
            var workerLen = this.isSoldier? this.house.soldiers.length : this.house.workers.length;
            var offset = (workerFactory.timeSinceStart / 5) + (this.houseIndex / workerLen) * Math.PI * 2;
            var target = new vector(this.house.transform.position.x + Math.sin(offset) * 100, this.house.transform.position.y + Math.cos(offset) * 100)
            this.transform.pointTowards(target);

            var speed = Math.min(this.moveSpeed, this.transform.position.distanceTo(target));

            this.moveForwards(deltaTime * speed);
        }
    },

    moveForwards: function(speed)
    {
        this.transform.position = this.transform.position.add(this.transform.forward().stretch(speed));
    },

    damage: function()
    {
        this.die();
    },

    die: function()
    {
        this.drop();
        this.destroy();
        GameObject.create(corpse, this.transform.position.x, this.transform.position.y, s_skull, 14)
    },

    findResourceByName: function(name)
    {
        var stored = false;
        var dist = 10000000;
        var found = null;

        for(var i = 0; i < GameObject.allObjects.length; i++)
        {
            if(GameObject.allObjects[i].name == name)
            {
                var cd = GameObject.allObjects[i].transform.position.distanceTo(this.transform.position);

                if(!this.task.blockStorage || !GameObject.allObjects[i].isStored)
                {
                    if(!GameObject.allObjects[i].isStored || GameObject.allObjects[i].storedTeam == this.team)
                    {
                        if(cd < dist && !GameObject.allObjects[i].isHeld)
                        {
                            if(stored)
                            {
                                if(GameObject.allObjects[i].isStored)
                                {
                                    dist = cd;
                                    found = GameObject.allObjects[i];
                                }
                            }
                            else
                            {
                                dist = cd;
                                found = GameObject.allObjects[i];
                                stored = GameObject.allObjects[i].isStored;
                            }
                        }
                    }
                }
            }
        }

        return found;
    },

    pickUp: function(object)
    {
        this.heldResource = object;
        this.heldResource.isHeld = true;
        this.heldResource.isStored = false;
    },

    drop: function()
    {
        if(this.heldResource)
        {
            this.heldResource.isHeld = false;
            this.heldResource = null;
        }
    },

    onDestroy: function()
    {
        
    },

    onCollision: function(other)
    {
        
    }
} 
 
/**
 * tasks for soldiers to perform
 */

 function workerAttackTask(targetToAttack, targetPosition, team, onComplete, context, needWeapon = true)
 {
     this.completed = false;
     this.assignedWorker = null;
     this.team = team;

     this.targetPosition = targetPosition;
     this.targetToAttack = targetToAttack
     this.onComplete = onComplete;
     this.needWeapon = needWeapon;

     this.update = function()
     {
         if(!this.completed)
         {
            if(this.assignedWorker && this.assignedWorker.destroyed)
                this.assignedWorker = null;

            if(!this.assignedWorker)
            {
                this.assignedWorker = workerFactory.findIdleWorkerByTeam(this.team, this.needWeapon);

                if(this.assignedWorker != null)
                    this.assignedWorker.task = this;
            }
            else
            {
                if(this.targetToAttack.destroyed || !context.IsInRange(this.targetToAttack, 500))
                {
                    var worker = this.assignedWorker;

                    this.completeTask();
                }
            }
        }
     }

     this.completeTask = function()
     {
         if(!this.completed)
         {
            this.onComplete && this.onComplete.call(context);

            this.assignedWorker.task = null;
            this.assignedWorker = null;

            this.completed = true;
         }
     }

     this.cancelTask = function()
     {
         if(!this.completed)
         {
             if(this.assignedWorker)
                this.assignedWorker.task = null;
                
            this.assignedWorker = null;

            this.completed = true;
         }
     }
 } 
 
/**
 * workerFactory spawns workers in the world on request
 */
var workerFactory = 
{
    start: function()
    {
        workerFactory.teams = [[],[],[],[]]; //max teams is 4
        workerFactory.timeSinceStart = 0;
    },

    findIdleWorkerByTeam: function(teamIndex, soldier = false)
    {
        for(var i = 0; i < workerFactory.teams[teamIndex].length; i++)
        {
            if(!workerFactory.teams[teamIndex][i].task && workerFactory.teams[teamIndex][i].hunger < workerFactory.teams[teamIndex][i].hungerFoodTrigger && workerFactory.teams[teamIndex][i].isSoldier == soldier)
                return workerFactory.teams[teamIndex][i];
        }

        return null;
    },

    spawnWorker: function(positionVector, teamIndex, hasWeapon = false)
    {
        var newWorker = GameObject.create(worker, positionVector.x, positionVector.y, (hasWeapon? s_soldier : s_worker), 15);
        newWorker.setTeam(teamIndex);

        newWorker.isSoldier = hasWeapon;

        workerFactory.teams[teamIndex].push(newWorker);

        return newWorker;
    },

    onDestroy: function()
    {
        for (var i = 0; i < gemSpawner.allGems.length; i++)
        {
            gemSpawner.allGems[i].destroy();
        }
    },

    update: function(deltaTime)
    {
        workerFactory.timeSinceStart += deltaTime;

        for(var t = 0; t < 4; t++)
        {
            workerFactory.teams[t] = workerFactory.teams[t].filter(function(item)
            {
                return !item.destroyed;
            });
        }
    }
} 
 
/**
 * tasks for workers to perform
 */

 function workerTask(targetResourceName, blockStorage, targetPosition, distance, team, onComplete, context, keepResource, needWeapon = false)
 {
     this.resourceName = targetResourceName;
     this.blockStorage = blockStorage;
     this.completed = false;
     this.assignedWorker = null;
     this.team = team;

     this.targetPosition = targetPosition;
     this.distance = distance;
     this.onComplete = onComplete;
     this.keepResource = keepResource === true;
     this.needWeapon = needWeapon;

     this.update = function()
     {
         if(!this.completed)
         {
            if(this.assignedWorker && this.assignedWorker.destroyed)
                this.assignedWorker = null;

            if(!this.assignedWorker)
            {
                this.assignedWorker = workerFactory.findIdleWorkerByTeam(this.team, this.needWeapon);

                if(this.assignedWorker != null)
                    this.assignedWorker.task = this;
            }
            else
            {
                if(this.assignedWorker.heldResource && this.assignedWorker.heldResource.name == this.resourceName)
                {
                    if(this.assignedWorker.transform.position.distanceTo(this.targetPosition) < this.distance)
                    {
                        var worker = this.assignedWorker;

                        this.completeTask();
                        
                        if(!this.keepResource)
                            worker.heldResource.destroy();

                        worker.heldResource = null;
                    }
                }
            }
        }
     }

     this.completeTask = function()
     {
         if(!this.completed)
         {
            this.onComplete && this.onComplete.call(context);

            this.assignedWorker.task = null;
            this.assignedWorker = null;

            this.completed = true;
         }
     }

     this.cancelTask = function()
     {
         if(!this.completed)
         {
             if(this.assignedWorker)
                this.assignedWorker.task = null;

            this.assignedWorker = null;

            this.completed = true;
         }
     }
 } 
 
