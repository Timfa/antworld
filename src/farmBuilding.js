/**
 * farmBuilding
 */

var farmBuilding =
{
    start: function()
    {
        this.targetFood = 4;
        this.foods = 0;

        this.base.start.call(this);

        this.foodSlots = [];
    },

    setTeam: function(team)
    {
        this.base.setTeam.call(this, team);

        for(var i = 0; i < this.targetFood; i++)
        {
            var offset = (workerFactory.timeSinceStart / 5) + (i / this.targetFood) * Math.PI * 2;
            var target = new vector(this.transform.position.x + Math.sin(offset) * 30, this.transform.position.y + Math.cos(offset) * 30);

            var newFoodSlot = GameObject.create(foodSlot, target.x, target.y, s_gemslot, 3);
            newFoodSlot.setTeam(this.team);

            newFoodSlot.storeMode = true;

            this.foodSlots.push(newFoodSlot);
        }
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.foodSlots.length; i++)
            this.foodSlots[i].destroy();
            
        this.base.onDestroy.call(this);
    }
}