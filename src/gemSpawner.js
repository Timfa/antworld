/**
 * Gem Spawner spawns gems in the world
 */
var gemSpawner = 
{
    start: function()
    {
        this.maxGems = 50;
        this.startGems = 25;

        gemSpawner.allGems = [];
        gemSpawner.wildGems = [];

        this.spawnInUpdate = true;
    },

    spawnGem: function(delay, enableSpawningAfter)
    {
        this.spawnInUpdate = false;

        delay = delay || 0;

        if(gemSpawner.allGems && gemSpawner.allGems.length > 0)
        {
            gemSpawner.allGems = gemSpawner.allGems.filter(function(item)
            {
                return !item.destroyed;
            });

            gemSpawner.wildGems = gemSpawner.allGems.filter(function(item)
            {
                return !item.destroyed && !item.isHeld && !item.isStored;
            });
        }

        timingUtil.delay(delay, function()
        {
            if (gemSpawner.wildGems.length < this.maxGems)
            {
                var pos = new vector(0, 0);
                var posFound = false;
                var tries = 0;

                while(!posFound)
                {
                    if(tries > 10)
                    {
                        this.spawnInUpdate = true;
                        return;
                    }

                    tries++;
                    pos.x = Math.random() * (GameWindow.width);
                    pos.y = Math.random() * (GameWindow.height);

                    var base = buildingManager.teams.random().random().transform.position;

                    pos = pos.add(base);
                    pos.x -= GameWindow.width/2;
                    pos.y -= GameWindow.height/2;

                    posFound = true;

                    if(gemSpawner.wildGems.length > 0 && Math.random() > 0.05)
                    {
                        var gemStart = gemSpawner.wildGems.random();

                        pos.x = - 30 + Math.random() * 60;
                        pos.y = - 30 + Math.random() * 60;

                        pos = pos.normalize();

                        pos = pos.stretch(25);
                        pos = pos.add(gemStart.transform.position);
                    }

                    /*if(pos.x < 0 || pos.y < 0 || pos.x > GameWindow.width || pos.y > GameWindow.height)
                        posFound = false;*/

                    for(var i = 0; i < gemSpawner.wildGems.length; i++)
                    {
                        if(gemSpawner.wildGems[i].transform.position.distanceTo(pos) < 24)
                            posFound = false;
                    }

                    for(var i = 0; i < buildingManager.allBuildings.length; i++)
                    {
                        if(buildingManager.allBuildings[i].transform.position.distanceTo(pos) < 150)
                            posFound = false;
                    }
                }

                gemSpawner.spawnGemAtPosition(pos);
            }

            if (enableSpawningAfter)
            {
                this.spawnInUpdate = true;
            }
        }, this);
    },

    spawnGemAtPosition: function(pos)
    {
        var ngem = GameObject.create(gem, pos.x, pos.y, s_gem, 5);
        gemSpawner.allGems.push(ngem);
        return ngem;
    },

    onDestroy: function()
    {
        for (var i = 0; i < gemSpawner.allGems.length; i++)
        {
            gemSpawner.allGems[i].destroy();
        }
    },

    update: function(deltaTime)
    {
        if (this.spawnInUpdate)
        {
            var percentage = gemSpawner.wildGems.length / this.maxGems;
            this.spawnGem((percentage * percentage * 500), true);
        }
    }
}