/**
 * Worker
 */

var worker =
{
    start: function()
    {
        this.hunger = 0;
        this.hungerFoodTrigger = 30;
        this.deathHunger = 60;

        this.age = 0;
        this.maxAge = 60 * 5;

        this.ignoreCollisions = true;

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        this.moveSpeed = 100;

        this.task = null;
        this.heldResource = null;
        this.attackTime = 1;
        this.attackTimer = 0;
        this.isSoldier = false;
        
        Object.defineProperty(this, "team", 
		{
			get: function()
			{
				return this.renderer.subImage;
			},

			set: function(val)
			{
				this.renderer.subImage = val;
			}
		})

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        GameAudio.play("bubble");
    },

    assignHouse: function(house)
    {
        this.house = house;
    },

    setTeam: function(team)
    {
        this.team = team;
    },

    update: function(deltaTime)
    {
        this.hunger += deltaTime;
        this.age += deltaTime;
        this.attackTimer -= deltaTime;

        if(this.house && this.house.destroyed)
            this.house = null;

        if(this.house == null)
        {
            for(var i = 0; i < buildingManager.teams[this.team].length; i++)
            {
                if(this.isSoldier)
                {
                    if(buildingManager.teams[this.team][i].soldiers && buildingManager.teams[this.team][i].soldiers.length < buildingManager.teams[this.team][i].maxSoldiers)
                    {
                        buildingManager.teams[this.team][i].soldiers.push(this);
                        this.house = buildingManager.teams[this.team][i];
                    }
                }
                else
                {
                    if(buildingManager.teams[this.team][i].workers && buildingManager.teams[this.team][i].workers.length < buildingManager.teams[this.team][i].maxWorkers)
                    {
                        buildingManager.teams[this.team][i].workers.push(this);
                        this.house = buildingManager.teams[this.team][i];
                    }
                }
            }
        }

        if(this.hunger > this.deathHunger || this.age > this.maxAge)
        {
            this.die();

            return;
        }

        if(this.age > 5 && this.hunger > this.hungerFoodTrigger && !this.task)
        {
            this.task = new workerTask("food", false, this.transform.position, 100, this.team, function()
            {
                this.hunger = 0;
                GameAudio.play("bite");
            }, this, false);

            this.task.selfUpdate = true;
            this.task.assignedWorker = this;
        }

        if(this.task && this.task.selfUpdate)
        {
            this.task.targetPosition = this.transform.position;
            
            this.task.update();
        }

       if(this.heldResource)
       {
           this.heldResource.transform.position = this.heldResource.transform.position.lerpTo(this.transform.position.add(this.transform.forward().stretch(30)), deltaTime * 10);
       }

       if(this.task)
       {
           if(this.task.needWeapon)
           {
                if(this.heldResource)
                {
                    this.drop();
                }

                if(this.transform.position.distanceTo(this.task.targetToAttack.transform.position) > ((this.task.targetToAttack.renderer.image.width / 2) + 24))
                {
                    this.transform.pointTowards(this.task.targetToAttack.transform.position);
                    this.moveForwards(deltaTime * this.moveSpeed * 1.1);
                }
                else
                {
                    if(this.attackTimer < 0)
                    {
                        this.attackTimer = this.attackTime;
                        this.task.targetToAttack.damage();
                    }
                    else
                    {
                        this.attackTimer -= deltaTime;
                    }
                }
           }
           else if(this.heldResource)
           {
               if(this.heldResource.name != this.task.resourceName)
               {
                    this.drop();
               }
               else
               {
                   this.transform.pointTowards(this.task.targetPosition);
                   this.moveForwards(deltaTime * this.moveSpeed);
               }
           }
           else
           {
               var targetResource = this.findResourceByName(this.task.resourceName);

               if(targetResource)
               {
                    this.transform.pointTowards(targetResource.transform.position);

                    if(this.transform.position.distanceTo(targetResource.transform.position) < 20)
                    {
                        this.pickUp(targetResource);
                    }

                    this.moveForwards(deltaTime * this.moveSpeed);
               }
               else
                this.idleBehaviour(deltaTime);
           }
       }
       else
       {
          this.idleBehaviour(deltaTime);
       }
    },

    idleBehaviour: function(deltaTime)
    {
        if(this.house)
        {
            var workerLen = this.isSoldier? this.house.soldiers.length : this.house.workers.length;
            var offset = (workerFactory.timeSinceStart / 5) + (this.houseIndex / workerLen) * Math.PI * 2;
            var target = new vector(this.house.transform.position.x + Math.sin(offset) * 100, this.house.transform.position.y + Math.cos(offset) * 100)
            this.transform.pointTowards(target);

            var speed = Math.min(this.moveSpeed, this.transform.position.distanceTo(target));

            this.moveForwards(deltaTime * speed);
        }
    },

    moveForwards: function(speed)
    {
        this.transform.position = this.transform.position.add(this.transform.forward().stretch(speed));
    },

    damage: function()
    {
        this.die();
    },

    die: function()
    {
        this.drop();
        this.destroy();
        GameObject.create(corpse, this.transform.position.x, this.transform.position.y, s_skull, 14)
    },

    findResourceByName: function(name)
    {
        var stored = false;
        var dist = 10000000;
        var found = null;

        for(var i = 0; i < GameObject.allObjects.length; i++)
        {
            if(GameObject.allObjects[i].name == name)
            {
                var cd = GameObject.allObjects[i].transform.position.distanceTo(this.transform.position);

                if(!this.task.blockStorage || !GameObject.allObjects[i].isStored)
                {
                    if(!GameObject.allObjects[i].isStored || GameObject.allObjects[i].storedTeam == this.team)
                    {
                        if(cd < dist && !GameObject.allObjects[i].isHeld)
                        {
                            if(stored)
                            {
                                if(GameObject.allObjects[i].isStored)
                                {
                                    dist = cd;
                                    found = GameObject.allObjects[i];
                                }
                            }
                            else
                            {
                                dist = cd;
                                found = GameObject.allObjects[i];
                                stored = GameObject.allObjects[i].isStored;
                            }
                        }
                    }
                }
            }
        }

        return found;
    },

    pickUp: function(object)
    {
        this.heldResource = object;
        this.heldResource.isHeld = true;
        this.heldResource.isStored = false;
    },

    drop: function()
    {
        if(this.heldResource)
        {
            this.heldResource.isHeld = false;
            this.heldResource = null;
        }
    },

    onDestroy: function()
    {
        
    },

    onCollision: function(other)
    {
        
    }
}