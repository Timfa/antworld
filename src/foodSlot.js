/**
 * foodSlot used on farms or storages
 */
var foodSlot = 
{
    start: function()
    {
        this.ignoreCollisions = true;
        this.team = -1;   
        this.task = null;
        this.storeMode = true;
    },

    setTeam: function(team)
    {
        this.team = team;
    },

    update: function(deltaTime)
    {
        if(this.team < 0)
            return;

        if(this.storeMode && this.contained && !this.contained.isStored)
        {
            this.contained = null;
        }

        if(!this.contained)
        {
            this.contained = foodSpawner.spawnFood(this.transform.position);
        }
        
        this.contained.isStored = true;
        this.contained.storedTeam = this.team;

        this.contained.isHeld == !this.contained.edible;
    },

    onDestroy: function()
    {
        if(this.task)
            this.task.cancelTask();
    }
}