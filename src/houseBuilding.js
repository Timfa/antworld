/**
 * houseBuilding
 */

var houseBuilding =
{
    start: function()
    {
        this.maxWorkers = 5;
        this.workers = [];

        this.base.start.call(this);
    },

    spawnWorker: function()
    {
        var newWorker = workerFactory.spawnWorker(this.transform.position, this.team);

        newWorker.assignHouse(this);

        this.workers.push(newWorker);

        for(var i = 0; i < this.workers.length; i++)
        {
            this.workers[i].houseIndex = i;
        }
    },

    update: function(deltaTime)
    {
        this.base.update.call(this, deltaTime);

        this.workers = this.workers.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.workers.length < this.maxWorkers)
        {
            if(this.spawnQueue.length < this.maxWorkers - this.workers.length)
            {
                this.spawnQueue.push(new workerTask("food", false, this.transform.position, 60, this.team, function()
                {
                    this.spawnWorker()
                }, this));
            }
        }
        else
        {
            for(var i = 0; i < this.spawnQueue.length; i++)
            {
                this.spawnQueue[i].cancelTask();
            }

            this.spawnQueue = [];
        }
    }
}