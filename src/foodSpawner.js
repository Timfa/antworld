/**
 * Food Spawner spawns food in the farms on request
 */
var foodSpawner = 
{
    start: function()
    {
        foodSpawner.allFood = [];

        this.spawnInUpdate = true;
    },

    spawnFood: function(pos)
    {
        var newFood = GameObject.create(food, pos.x, pos.y, s_food, 5);
        foodSpawner.allFood.push(newFood);

        return newFood;
    },

    onDestroy: function()
    {
        for (var i = 0; i < foodSpawner.allFood.length; i++)
        {
            foodSpawner.allFood[i].destroy();
        }
    },

    update: function(deltaTime)
    {
        foodSpawner.allFood = foodSpawner.allFood.filter(function(item)
        {
            return !item.destroyed;
        });
    }
}