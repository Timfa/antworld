/**
 * tasks for soldiers to perform
 */

 function workerAttackTask(targetToAttack, targetPosition, team, onComplete, context, needWeapon = true)
 {
     this.completed = false;
     this.assignedWorker = null;
     this.team = team;

     this.targetPosition = targetPosition;
     this.targetToAttack = targetToAttack
     this.onComplete = onComplete;
     this.needWeapon = needWeapon;

     this.update = function()
     {
         if(!this.completed)
         {
            if(this.assignedWorker && this.assignedWorker.destroyed)
                this.assignedWorker = null;

            if(!this.assignedWorker)
            {
                this.assignedWorker = workerFactory.findIdleWorkerByTeam(this.team, this.needWeapon);

                if(this.assignedWorker != null)
                    this.assignedWorker.task = this;
            }
            else
            {
                if(this.targetToAttack.destroyed || !context.IsInRange(this.targetToAttack, 500))
                {
                    var worker = this.assignedWorker;

                    this.completeTask();
                }
            }
        }
     }

     this.completeTask = function()
     {
         if(!this.completed)
         {
            this.onComplete && this.onComplete.call(context);

            this.assignedWorker.task = null;
            this.assignedWorker = null;

            this.completed = true;
         }
     }

     this.cancelTask = function()
     {
         if(!this.completed)
         {
             if(this.assignedWorker)
                this.assignedWorker.task = null;
                
            this.assignedWorker = null;

            this.completed = true;
         }
     }
 }