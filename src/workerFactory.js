/**
 * workerFactory spawns workers in the world on request
 */
var workerFactory = 
{
    start: function()
    {
        workerFactory.teams = [[],[],[],[]]; //max teams is 4
        workerFactory.timeSinceStart = 0;
    },

    findIdleWorkerByTeam: function(teamIndex, soldier = false)
    {
        for(var i = 0; i < workerFactory.teams[teamIndex].length; i++)
        {
            if(!workerFactory.teams[teamIndex][i].task && workerFactory.teams[teamIndex][i].hunger < workerFactory.teams[teamIndex][i].hungerFoodTrigger && workerFactory.teams[teamIndex][i].isSoldier == soldier)
                return workerFactory.teams[teamIndex][i];
        }

        return null;
    },

    spawnWorker: function(positionVector, teamIndex, hasWeapon = false)
    {
        var newWorker = GameObject.create(worker, positionVector.x, positionVector.y, (hasWeapon? s_soldier : s_worker), 15);
        newWorker.setTeam(teamIndex);

        newWorker.isSoldier = hasWeapon;

        workerFactory.teams[teamIndex].push(newWorker);

        return newWorker;
    },

    onDestroy: function()
    {
        for (var i = 0; i < gemSpawner.allGems.length; i++)
        {
            gemSpawner.allGems[i].destroy();
        }
    },

    update: function(deltaTime)
    {
        workerFactory.timeSinceStart += deltaTime;

        for(var t = 0; t < 4; t++)
        {
            workerFactory.teams[t] = workerFactory.teams[t].filter(function(item)
            {
                return !item.destroyed;
            });
        }
    }
}