/**
 * Buildsite that will transform into a building when completed
 */

var buildSite =
{
    start: function()
    {
        this.targetGems = 10;
        this.gems = 0;

        this.base.start.call(this);

        this.gemSlots = [];

        this.buildingInProgress = false;
    },

    setBuildingType: function(type, image, cost, team, onComplete, context)
    {
        this.targetGems = cost;
        this.targetBuilding = type;
        this.targetBuildingImage = image;
        this.setTeam(team);

        this.onComplete = onComplete;
        this.completeContext = context;

        for(var i = 0; i < cost; i++)
        {
            var offset = (workerFactory.timeSinceStart / 5) + (i / cost) * Math.PI * 2;
            var target = new vector(this.transform.position.x + Math.sin(offset) * 30, this.transform.position.y + Math.cos(offset) * 30);

            var newGemSlot = GameObject.create(gemSlot, target.x, target.y, s_gemslot, 3);
            newGemSlot.setTeam(this.team);

            this.gemSlots.push(newGemSlot);
        }
    },

    update: function(deltaTime)
    {
        var canBuild = true;

        for(var i = 0; i < this.gemSlots.length; i++)
        {
            if(!this.gemSlots[i].contained)
            {
                canBuild = false;
            }
        }

        if(canBuild && !this.buildingInProgress)
        {
            this.buildingInProgress = true;

            for(var i2 = 0; i2 < this.gemSlots.length; i2++)
            {
                var index = i2;
                timingUtil.delay(index * 200, function(index){return function()
                {
                    var scaleUpTween = new TWEEN.Tween(this.gemSlots[index].transform.scale);
                    scaleUpTween.to({x: [1.5, 0], y: [1.5, 0]}, 100);
                    scaleUpTween.easing(TWEEN.Easing.Back.In);
                    scaleUpTween.start();

                    var scaleUpTween2 = new TWEEN.Tween(this.gemSlots[index].contained.transform.scale);
                    scaleUpTween2.to({x: [1.5, 0], y: [1.5, 0]}, 100);
                    scaleUpTween2.easing(TWEEN.Easing.Back.In);
                    scaleUpTween2.start();
                }}(index), this);
            }

            timingUtil.delay(this.gemSlots.length * 100, function()
            {
                var scaleUpTween = new TWEEN.Tween(this.transform.scale);
                scaleUpTween.to({x: [1.5, 0], y: [1.5, 0]}, 800);
                scaleUpTween.easing(TWEEN.Easing.Back.In);
                scaleUpTween.start();

                for(var i = 0; i < this.gemSlots.length; i++)
                {
                    this.gemSlots[i].contained.destroy();
                    this.gemSlots[i].destroy();
                }

                timingUtil.delay(800, function()
                {
                    var building = GameObject.create(this.targetBuilding, this.transform.position.x, this.transform.position.y, this.targetBuildingImage);
                    building.setHealth(this.targetGems * 5);
                    building.setTeam(this.team);
                    this.destroy();

                    this.onComplete.call(this.completeContext, building);
                }, this);
            }, this);
        }
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.gemSlots.length; i++)
            this.gemSlots[i].destroy();
            
        this.base.onDestroy.call(this);
    }
}