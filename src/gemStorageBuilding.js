/**
 * gemStorageBuilding
 */

var gemStorageBuilding =
{
    start: function()
    {
        this.targetGems = 6;
        this.gems = 0;

        this.base.start.call(this);

        this.gemSlots = [];
    },

    setTeam: function(team)
    {
        this.base.setTeam.call(this, team);

        for(var i = 0; i < this.targetGems; i++)
        {
            var offset = (workerFactory.timeSinceStart / 5) + (i / this.targetGems) * Math.PI * 2;
            var target = new vector(this.transform.position.x + Math.sin(offset) * 30, this.transform.position.y + Math.cos(offset) * 30);

            var newGemSlot = GameObject.create(gemSlot, target.x, target.y, s_gemslot, 3);
            newGemSlot.setTeam(this.team);

            newGemSlot.storeMode = true;

            this.gemSlots.push(newGemSlot);
        }
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.gemSlots.length; i++)
            this.gemSlots[i].destroy();
            
        this.base.onDestroy.call(this);
    }
}