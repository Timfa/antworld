/**
 * All sounds in the game
 */

var soundsFolder = "sounds";
var sounds = 
[
	{
        name: "bubble",
        volume: 1
    },
    {
        name: "glass",
        volume: 0.9
    },
    {
        name: "tap",
        volume: 1
    },
    {
        name: "bite",
        volume: 1
    },
    {
        name: "pop",
        volume: 0.1
    }

]; 