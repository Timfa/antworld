/**
 * tasks for workers to perform
 */

 function workerTask(targetResourceName, blockStorage, targetPosition, distance, team, onComplete, context, keepResource, needWeapon = false)
 {
     this.resourceName = targetResourceName;
     this.blockStorage = blockStorage;
     this.completed = false;
     this.assignedWorker = null;
     this.team = team;

     this.targetPosition = targetPosition;
     this.distance = distance;
     this.onComplete = onComplete;
     this.keepResource = keepResource === true;
     this.needWeapon = needWeapon;

     this.update = function()
     {
         if(!this.completed)
         {
            if(this.assignedWorker && this.assignedWorker.destroyed)
                this.assignedWorker = null;

            if(!this.assignedWorker)
            {
                this.assignedWorker = workerFactory.findIdleWorkerByTeam(this.team, this.needWeapon);

                if(this.assignedWorker != null)
                    this.assignedWorker.task = this;
            }
            else
            {
                if(this.assignedWorker.heldResource && this.assignedWorker.heldResource.name == this.resourceName)
                {
                    if(this.assignedWorker.transform.position.distanceTo(this.targetPosition) < this.distance)
                    {
                        var worker = this.assignedWorker;

                        this.completeTask();
                        
                        if(!this.keepResource)
                            worker.heldResource.destroy();

                        worker.heldResource = null;
                    }
                }
            }
        }
     }

     this.completeTask = function()
     {
         if(!this.completed)
         {
            this.onComplete && this.onComplete.call(context);

            this.assignedWorker.task = null;
            this.assignedWorker = null;

            this.completed = true;
         }
     }

     this.cancelTask = function()
     {
         if(!this.completed)
         {
             if(this.assignedWorker)
                this.assignedWorker.task = null;

            this.assignedWorker = null;

            this.completed = true;
         }
     }
 }