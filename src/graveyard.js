/**
 * graveyardBuilding to store skulls in
 */

var graveyardBuilding =
{
    start: function()
    {
        this.targetSkulls = 6;
        this.skulls = 0;

        this.base.start.call(this);

        this.skullSlots = [];
    },

    setTeam: function(team)
    {
        this.base.setTeam.call(this, team);

        for(var i = 0; i < this.targetSkulls; i++)
        {
            var offset = (workerFactory.timeSinceStart / 5) + (i / this.targetSkulls) * Math.PI * 2;
            var target = new vector(this.transform.position.x + Math.sin(offset) * 30, this.transform.position.y + Math.cos(offset) * 30);

            var newSkullSlot = GameObject.create(skullSlot, target.x, target.y, s_gemslot, 3);
            newSkullSlot.setTeam(this.team);

            newSkullSlot.storeMode = true;

            this.skullSlots.push(newSkullSlot);
        }
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.skullSlots.length; i++)
            this.skullSlots[i].destroy();
            
        this.base.onDestroy.call(this);
    }
}