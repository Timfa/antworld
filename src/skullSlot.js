/**
 * skullSlot used on storages
 */
var skullSlot = 
{
    start: function()
    {
        this.ignoreCollisions = true;
        this.team = -1;   
        this.task = null;
        this.storeMode = true;
    },

    setTeam: function(team)
    {
        this.team = team;

        this.task = new workerTask("skull", this.storeMode, this.transform.position, 15, this.team, function()
        {
            this.contained = this.task.assignedWorker.heldResource;
            this.contained.transform.position = this.transform.position;

            this.contained.isHeld = true;

            timingUtil.delay(100, function()
            {
                this.contained.isHeld = !this.storeMode;
            }, this);

            this.contained.isStored = this.storeMode;
            this.contained.storedTeam = this.team;

            if(!this.storeMode)
                this.task = null;

        }, this, true);
    },

    update: function(deltaTime)
    {
        if(this.task)
            this.task.blockStorage = this.storeMode;

        if(this.storeMode && this.contained && !this.contained.isStored && this.transform.position.distanceTo(this.contained.transform.position) > 10)
        {
            this.contained = null;
            this.task.completed = false;
        }

        if(this.contained && this.contained.destroyed)
        {
            var ngem = gemSpawner.spawnGemAtPosition(this.transform.position);

            this.contained = ngem;
            this.contained.isStored = false;
            this.contained.team = this.team;
            this.contained.isHeld = false;
        }

        if(this.task && !this.contained)
        {
            this.task.update();
        }
    },

    onDestroy: function()
    {
        if(this.task)
            this.task.cancelTask();
    }
}