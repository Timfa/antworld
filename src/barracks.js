/**
 * houseBuilding
 */

var barracks =
{
    start: function()
    {
        this.maxSoldiers = 5;
        this.soldiers = [];

        this.trackedTargets = [];

        this.base.start.call(this);
    },

    spawnWorker: function()
    {
        var newWorker = workerFactory.spawnWorker(this.transform.position, this.team, true);

        newWorker.assignHouse(this);

        this.soldiers.push(newWorker);

        for(var i = 0; i < this.soldiers.length; i++)
        {
            this.soldiers[i].houseIndex = i;
        }
    },

    IsInRange: function(worker, range)
    {
        if(buildingManager == null)
            return false;

        for(var b = 0; b < buildingManager.teams[this.team].length; b++)
        {
            if(worker.transform.position.distanceTo(buildingManager.teams[this.team][b].transform.position) < 250)
                return true;
        }

        return false;
    },

    update: function(deltaTime)
    {
        this.base.update.call(this, deltaTime);

        this.soldiers = this.soldiers.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.soldiers.length < this.maxSoldiers)
        {
            if(this.spawnQueue.length < this.maxSoldiers - this.soldiers.length)
            {
                this.spawnQueue.push(new workerTask("food", false, this.transform.position, 60, this.team, function()
                {
                    this.spawnWorker()
                }, this));
            }
        }
        else
        {
            for(var i = 0; i < this.spawnQueue.length; i++)
            {
                this.spawnQueue[i].cancelTask();
            }

            this.spawnQueue = [];
        }
    }
}