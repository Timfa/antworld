/**
 * hqBuilding
 */

var hqBuilding =
{
    start: function()
    {
        this.maxWorkers = 8;
        this.workers = [];

        this.base.start.call(this);

        this.buildSites = [];
        this.houses = [];
        this.barracks = [];
        this.gemStores = [];
        this.graveyards = [];
        this.farms = [];
        this.readyForTasks = false;

        this.trackedTargets = [];

        this.buildCooldown = 0;

        timingUtil.delay(1000, function()
        {
            this.spawnWorker();
            timingUtil.delay(1500, function()
            {
                this.readyForTasks = true;
                console.log("ready for tasks");
            }, this);
        }, this);

    },

    IsInRange: function(worker, range)
    {
        if(buildingManager == null)
            return false;

        for(var b = 0; b < buildingManager.teams[this.team].length; b++)
        {
            if(worker.transform.position.distanceTo(buildingManager.teams[this.team][b].transform.position) < range)
                return true;
        }

        return false;
    },

    spawnWorker: function()
    {
        var newWorker = workerFactory.spawnWorker(this.transform.position, this.team);

        newWorker.assignHouse(this);

        this.workers.push(newWorker);

        for(var i = 0; i < this.workers.length; i++)
        {
            this.workers[i].houseIndex = i;
        }
    },

    handleDefense: function()
    {
        var context = this;
        this.trackedTargets = this.trackedTargets.filter(function(item)
        {
            return !item.destroyed && context.IsInRange(item, 450);
        });

        for(var t = 0; t < 4; t++)
        {
            if(t != this.team)
            {
                for(var i = 0; i < workerFactory.teams[t].length; i++)
                {
                    if(this.trackedTargets.indexOf(workerFactory.teams[t][i]) < 0)
                    {
                        if(this.IsInRange(workerFactory.teams[t][i], 150))
                        {
                            this.trackedTargets.push(workerFactory.teams[t][i]);

                            this.taskList.push(new workerAttackTask(workerFactory.teams[t][i], workerFactory.teams[t][i].transform.position, this.team, function()
                            {
                                
                            }, this));
                        }
                    }
                }

                for(var i = 0; i < buildingManager.teams[t].length; i++)
                {
                    if(this.trackedTargets.indexOf(buildingManager.teams[t][i]) < 0)
                    {
                        if(this.IsInRange(buildingManager.teams[t][i], 450))
                        {
                            this.trackedTargets.push(buildingManager.teams[t][i]);

                            for(var s = 0; s < 10; s++)
                            {
                                this.taskList.push(new workerAttackTask(buildingManager.teams[t][i], buildingManager.teams[t][i].transform.position, this.team, function()
                                {
                                    
                                }, this));
                            }
                        }
                    }
                }
            }
        }
    },

    update: function(deltaTime)
    {
        this.buildCooldown -= deltaTime;

        this.base.update.call(this, deltaTime);

        this.buildSites = this.buildSites.filter(function(item)
        {
            return !item.destroyed;
        });

        this.workers = this.workers.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.workers.length < this.maxWorkers && this.readyForTasks)
        {
            if(this.taskList.length < this.maxWorkers - this.workers.length)
            {
                this.taskList.push(new workerTask("food", false, this.transform.position, 60, this.team, function()
                {
                    this.spawnWorker()
                }, this, false));
            }
        }

        this.tryBuild();
        
        if(workerFactory != null && this.barracks.length > 0)
            this.handleDefense();
    },

    tryBuild: function()
    {
        if(this.buildCooldown > 0)
            return;

        if(this.shouldBuildHouse())
        {
            this.makeBuildSite(houseBuilding, s_house, 5, function(newHouse)
            {
                this.houses.push(newHouse);
                this.buildCooldown = 3;
            });
        }

        if(this.shouldBuildBarracks())
        {
            this.makeBuildSite(barracks, s_barracks, 6, function(newHouse)
            {
                this.barracks.push(newHouse);
                this.buildCooldown = 3;
            });
        }

        if(this.shouldBuildGemStorage())
        {
            this.makeBuildSite(gemStorageBuilding, s_gemStorage, 3, function(newHouse)
            {
                this.gemStores.push(newHouse);
                this.buildCooldown = 3;
            });
        }

        if(this.shouldBuildFarm())
        {
            this.makeBuildSite(farmBuilding, s_farm, 2, function(newHouse)
            {
                this.farms.push(newHouse);
                this.buildCooldown = 3;
            });
        }

        if(this.shouldBuildGraveyard())
        {
            this.makeBuildSite(graveyardBuilding, s_graveyard, 4, function(newHouse)
            {
                this.graveyards.push(newHouse);
                this.buildCooldown = 3;
            });
        }
    },

    makeBuildSite: function(type, image, cost, onComplete)
    {
        var position = this.findBuildSite();

        if(!position)
            return;

        var newBuildSite = GameObject.create(buildSite, position.x, position.y, s_base, -1);

        newBuildSite.setBuildingType(type, image, cost, this.team, onComplete, this);

        this.buildSites.push(newBuildSite);
    },

    findBuildSite: function()
    {
        var pos = new vector(0, 0);
        var posFound = false;
        var tries = 0;

        while(!posFound)
        {
            if(tries > 100)
                return null;

            posFound = true;
            tries++;

            var buildingStart = buildingManager.teams[this.team].random();

            pos.x = - 75 + Math.random() * 150;
            pos.y = - 75 + Math.random() * 150;

            pos = pos.normalize();

            pos = pos.stretch(240);
            pos = pos.add(buildingStart.transform.position);

           /* if(pos.x < 200 || pos.y < 200 || pos.x > GameWindow.width - 200 || pos.y > GameWindow.height - 200)
                posFound = false;*/

            for(var i = 0; i < gemSpawner.wildGems.length; i++)
            {
                if(gemSpawner.wildGems[i].transform.position.distanceTo(pos) < 99)
                    posFound = false;
            }

            for(var i = 0; i < buildingManager.allBuildings.length; i++)
            {
                if(buildingManager.allBuildings[i].transform.position.distanceTo(pos) < 239)
                    posFound = false;
            }
        }

        return pos;
    },

    shouldBuildHouse: function()
    {
        this.houses = this.houses.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.workers.length < this.maxWorkers)
            return false;

        for(var i = 0; i < this.houses.length; i++)
        {
            if(this.houses[i].workers.length < this.houses[i].maxWorkers)
                return false;
        }

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == houseBuilding)
                return false;
        }

        return true;
    },

    shouldBuildBarracks: function()
    {
        this.barracks = this.barracks.filter(function(item)
        {
            return !item.destroyed;
        });

        if(this.workers.length < this.maxWorkers)
            return false;

        for(var i = 0; i < this.barracks.length; i++)
        {
            if(this.barracks[i].soldiers.length < this.barracks[i].maxSoldiers)
                return false;
        }

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == barracks)
                return false;
        }

        return true;
    },

    shouldBuildGemStorage: function()
    {
        this.gemStores = this.gemStores.filter(function(item)
        {
            return !item.destroyed;
        });

        var workerCount = workerFactory.teams[this.team].length; 
        
        if(workerCount < 3)
            return false;

        for(var i = 0; i < this.gemStores.length; i++)
        {
            for(var s = 0; s < this.gemStores[i].gemSlots.length; s++)
            {
                if(!this.gemStores[i].gemSlots[s].contained)
                    return false;
            }
        }

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == gemStorageBuilding)
                return false;
        }

        return true;
    },

    shouldBuildGraveyard: function()
    {
        this.graveyards = this.graveyards.filter(function(item)
        {
            return !item.destroyed;
        });

        var workerCount = workerFactory.teams[this.team].length; 
        
        if(workerCount < 10)
            return false;

        for(var i = 0; i < this.graveyards.length; i++)
        {
            for(var s = 0; s < this.graveyards[i].skullSlots.length; s++)
            {
                if(!this.graveyards[i].skullSlots[s].contained)
                    return false;
            }
        }

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == graveyardBuilding)
                return false;
        }

        return true;
    },

    setTeam: function(team)
    {
        this.base.setTeam.call(this, team);

        var newFoodSlot = GameObject.create(foodSlot, this.transform.position.x + 35, this.transform.position.y + 35, s_gemslot, 3);
        newFoodSlot.setTeam(this.team);

        newFoodSlot.storeMode = true;
    },

    shouldBuildFarm: function()
    {
        this.farms = this.farms.filter(function(item)
        {
            return !item.destroyed;
        });

        var workerCount = workerFactory.teams[this.team].length; 
 
        var totalSlots = 0;

        for(var i = 0; i < this.farms.length; i++) 
        { 
            totalSlots += this.farms[i].foodSlots.length; 
        }

        if(totalSlots >= (workerCount / 4) * 3) 
            return false;

        for(var i = 0; i < this.buildSites.length; i++)
        {
            if(this.buildSites[i].targetBuilding == farmBuilding)
                return false;
        }

        return true;
    }
}