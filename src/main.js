/**
 * Main script
 */

window.onload = init;
var pW = 0;
var pH = 0;

function DoInheritance()
{
    buildSite = GameObject.inherit(building, buildSite);
    gemStorageBuilding = GameObject.inherit(building, gemStorageBuilding);
    farmBuilding = GameObject.inherit(building, farmBuilding);
    barracks = GameObject.inherit(building, barracks);
    houseBuilding = GameObject.inherit(building, houseBuilding);
    hqBuilding = GameObject.inherit(building, hqBuilding);
    graveyardBuilding = GameObject.inherit(building, graveyardBuilding);
}

function init()
{
    DoInheritance();
    
    var width = window.innerWidth - 2;
    var height = window.innerHeight - 2;

    Game.initialize(0, 0, width, height, gameStart);
}

function gameStart()
{
	GameWindow.backgroundColor = {r:100, g:149, b:237};

	Game.setUpdateFunction(update);

    GameAudio.init(sounds, soundsFolder);
    GameObject.create(workerFactory, 500, 500);

    var b1 = GameObject.create(hqBuilding, 200, 200, s_hq);
    var b2 = GameObject.create(hqBuilding, GameWindow.width - 200, GameWindow.height - 200, s_hq);
    var b3 = GameObject.create(hqBuilding, 200, GameWindow.height - 200, s_hq);
    var b4 = GameObject.create(hqBuilding, GameWindow.width - 200, 200, s_hq);

    b1.setTeam(0);
    b2.setTeam(1);
    b3.setTeam(2);
    b4.setTeam(3);
    
    GameObject.create(gemSpawner, 500, 500);
    GameObject.create(foodSpawner, 500, 500);

    console.log("STARTED");
}

function update(deltaTime)
{  
   if(GameInput.isHeld(37))
    GameWindow.cameraPosition.x += deltaTime * 200;

    if(GameInput.isHeld(38))
    GameWindow.cameraPosition.y += deltaTime * 200;

    if(GameInput.isHeld(39))
    GameWindow.cameraPosition.x -= deltaTime * 200;

    if(GameInput.isHeld(40))
    GameWindow.cameraPosition.y -= deltaTime * 200;

    if(GameInput.mouseHeld(2))
    {
        GameWindow.cameraPosition = GameWindow.cameraPosition.subtract(new vector(GameInput.trueMousePosition.x - GameWindow.width/2, GameInput.trueMousePosition.y - GameWindow.height/2).stretch(deltaTime * 3));
    }

    if(GameInput.mousePressed(1))
    GameWindow.cameraPosition = new vector(0, 0);

    if(pW != window.innerWidth - 2)
    {
        pW = window.innerWidth - 2;
        GameWindow.g_width = window.innerWidth - 2;
    }

    if(pH != window.innerHeight - 2)
    {
        pH = window.innerHeight - 2;
        GameWindow.g_height = window.innerHeight - 2;
    }
}