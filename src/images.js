
/***
 * All images used in the game are  initialized here
 */

 var s_worker = new image("images/snakeHead.png", 4, 4);
 var s_soldier = new image("images/snakeHeadWeapon.png", 4, 4);
var s_food = new image("images/snakeBody.png", 4, 4);
var s_base = new image("images/bases.png", 4, 4);
var s_gem = new image("images/gems.png", 72, 6);
var s_playButton = new image("images/playbutton.png");

var s_playerCountText = new image("images/players.png");
var s_botsOnlyText = new image("images/botsonly.png");
var s_playerCountNumbers = new image("images/playercount.png", 3, 3);


var s_hq = new image("images/buildings/hq.png", 4, 4);
var s_house = new image("images/buildings/house.png", 4, 4);
var s_barracks = new image("images/buildings/barracks.png", 4, 4);
var s_gemStorage = new image("images/buildings/gemstorage.png", 4, 4);
var s_graveyard = new image("images/buildings/graveyard.png", 4, 4);
var s_farm = new image("images/buildings/farm.png", 4, 4);
var s_gemslot = new image("images/gemSlot.png");
var s_skull = new image("images/skull.png");