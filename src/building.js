/**
 * building
 */

var building =
{
    setHealth: function(health)
    {
        this.health = health;
        this.maxHealth = health;
    },

    start: function()
    {
        this.ignoreCollisions = true;
        this.health = 10;
        this.maxHealth = 10;

        if(!buildingManager.allBuildings)
        {
            buildingManager.allBuildings = [];
        }

        buildingManager.allBuildings.push(this);

        this.taskList = [];
        this.spawnQueue = [];

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        Object.defineProperty(this, "team", 
		{
			get: function()
			{
				return this.renderer.subImage;
			},

			set: function(val)
			{
				this.renderer.subImage = val;
			}
		})

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        GameAudio.play("bubble");
    },

    damage: function()
    {
        this.health -= 1;

        this.taskList.push(new workerTask("gem", false, this.transform.position, 60, this.team, function()
        {
            this.health += 1;
        }, this));

        if(this.health <= 0)
        {
            this.destroy();
        }
    },

    update: function(deltaTime)
    {
        this.taskList = this.taskList.filter(function(item)
        {
            return !item.completed;
        });

        for(var i = 0; i < this.taskList.length; i++)
        {
            this.taskList[i].update();
        }

        this.spawnQueue = this.spawnQueue.filter(function(item)
        {
            return !item.completed;
        });

        for(var i = 0; i < this.spawnQueue.length; i++)
        {
            this.spawnQueue[i].update();
        }
    },

    setTeam: function(team)
    {
        this.team = team;
        
        buildingManager.teams[this.team].push(this);
    },

    onDestroy: function()
    {
        for(var i = 0; i < this.taskList.length; i++)
        {
            this.taskList[i].cancelTask();
        }

        if(this.spawnQueue)
        {
            for(var i = 0; i < this.spawnQueue.length; i++)
            {
                this.spawnQueue[i].cancelTask();
            }
        }

        buildingManager.teams[this.team].remove(this);

        buildingManager.allBuildings.remove(this);

        this.taskList = [];
    }
}