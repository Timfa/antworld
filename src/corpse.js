/**
 * dead worker
 */

var corpse =
{
    start: function()
    {
        this.name = "skull";
        this.ignoreCollisions = true;

        timingUtil.delay(60 * 3 * 1000, function()
        {
            var scaleUpTween = new TWEEN.Tween(this.transform.scale);
            scaleUpTween.to({x: [1.5, 0], y: [1.5, 0]}, 800);
            scaleUpTween.easing(TWEEN.Easing.Back.In);
            scaleUpTween.start();

            timingUtil.delay(800, function()
            {
                this.destroy();
            }, this);
        }, this);
    }
}